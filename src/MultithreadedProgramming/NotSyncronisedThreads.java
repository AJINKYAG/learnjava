package MultithreadedProgramming;

public class NotSyncronisedThreads {
	public static void main (String args[])
	{
		Callme target = new Callme();
		Caller obj1 = new Caller(target,"Hello");
		Caller obj2 = new Caller(target,"Synchronised");
		Caller obj3 = new Caller (target,"world");
		
		try {
			obj1.t.join();
			obj2.t.join();
			obj3.t.join();
			
		}
		catch(InterruptedException e ) {
			System.out.println(e);
		}
	}
	
}



class Callme{
	synchronized void call (String message){
		System.out.print("[ " + message );
		try {
			Thread.sleep(1000);
		}
		catch(InterruptedException e) {
			System.out.println(e);
		}
		
		System.out.println(" ] ");
	}
}



class Caller implements Runnable{
	String message;
	Callme target;
	Thread t;
	public Caller(Callme targ,String msg ) {
		target = targ;
		message = msg;
		t = new Thread(this);
		t.start();
	}
	
	
	@Override
	public void run() {
			target.call(message);
	}
	
}


