package MultithreadedProgramming;

//Controlling the main thread
public class CurrentThreadDemo {
	public static void main (String args[]) throws InterruptedException {
		System.out.println("I am in main");
		Thread t = Thread.currentThread();
		
		System.out.println("Current Thread is : " + t.getName());
		t.setName("Aji_Thread");
		System.out.println("Current Thread is : " + t.getName());
		
		
		try {
			for (int i = 5 ; i > 0 ; i--) {
				System.out.println("Value of i is: " + i);
				Thread.sleep(10000);
				
			}
		}
		catch (InterruptedException e) {
			System.out.println( e );
		}
			
	}

		
}
