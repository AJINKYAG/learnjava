package MultithreadedProgramming;

public class JoinDemo {
public static void main (String agrs []) {
	NewThread4 thread1 = new NewThread4("firstThread");
	NewThread4 thread2 = new NewThread4("secondThread");
	NewThread4 thread3 = new NewThread4("thirdThread");
	
	Thread t = Thread.currentThread();
	System.out.println("Current Thread is : " + t.getName());
	
	
	try {
		for (int i = 5 ; i > 0 ; i--) {
			System.out.println(" inside thread main Value of i is: " + i);
			thread1.t.join();
			thread2.t.sleep(4000);
			thread2.t.join();
			thread3.t.join();
			Thread.sleep(10000);
			
		}
		
	}
	catch (InterruptedException e) {
		System.out.println( e );
	}
	if (!(thread1.t.isAlive() && thread2.t.isAlive() && thread3.t.isAlive())) 
		System.out.println("Main thread complete");
}
}


class NewThread4 implements Runnable {
	String threadName;
	Thread t ;
	NewThread4(String threadName){
		t = new Thread(this, threadName);
		System.out.println("Inside Child Thread: " + t.getName());
		t.start();
	}
	
	
	@Override
	public void run() {
		
		for (int i = 0; i < 6 ;i ++) {
			System.out.println("Inside thread " + t.getName() + "with i = " + i);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Compltion of thread " + t.getName());
	}
}

