package MultithreadedProgramming;

public class HIghLowPriority {
	public static void main (String args[]) {
		Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		System.out.println("Priority of " + Thread.currentThread() + " is " + Thread.currentThread().getPriority());
		Clicker high = new Clicker(Thread.NORM_PRIORITY +2, "highPriorityThread");
		
		Clicker low = new Clicker(Thread.NORM_PRIORITY - 2,"lowPriorityThread");
		
		high.start();
		low.start();
		
		try {
			System.out.println("Priority of " + high.t.getName() + " is " + high.t.getPriority());
			System.out.println("Priority of " + low.t.getName() + " is " + low.t.getPriority());
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		high.stop();
		low.stop();
		
		try {
			high.t.join();
			low.t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Low Priority Thread Ran " + low.click + "times");
		System.out.println("HIgh Priority Thread Ran " + high.click + "times");
	}
}


class Clicker implements Runnable{
	long click = 0;
	String threadName;
	Thread t;
	private volatile boolean running = true;
	
	public Clicker( int p,String threadName ) {
		t = new Thread(this,threadName );
		t.setPriority(p);
	}
	
	
	@Override
	public void run() {
		System.out.println("Run Methods");
		while(running) {
			click++;
		}
		
	}
	public void stop() {
		running = false;
	}
	
	public void start() {
		t.start();
		System.out.println(" thread " + t.getName() + " Started ");
	}
}      