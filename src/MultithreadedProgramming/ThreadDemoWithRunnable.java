package MultithreadedProgramming;

public class ThreadDemoWithRunnable {
public static void main (String agrs []) {
	new NewThread();
	
	System.out.println("I am in main");
	Thread t = Thread.currentThread();
	
	System.out.println("Current Thread is : " + t.getName());
	t.setName("Aji_Thread");
	System.out.println("Current Thread is : " + t.getName());
	
	
	try {
		for (int i = 5 ; i > 0 ; i--) {
			System.out.println(" inside thread main Value of i is: " + i);
			Thread.sleep(10000);
		}
		
	}
	catch (InterruptedException e) {
		System.out.println( e );
	}
	System.out.println("Main thread complete");
}
}


class NewThread implements Runnable {
	
	Thread t ;
	NewThread(){
		t = new Thread(this, "Child_Thread");
		System.out.println("Inside Child Thread: " + t.getName());
		t.start();
	}
	
	
	@Override
	public void run() {
		
		for (int i = 0; i < 6 ;i ++) {
			System.out.println("Inside thread " + t.getName() + "with i = " + i);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

