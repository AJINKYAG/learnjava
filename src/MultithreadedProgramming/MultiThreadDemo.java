package MultithreadedProgramming;

public class MultiThreadDemo {
public static void main (String agrs []) {
	new NewThread3("firstThread");
	new NewThread3("secondThread");
	new NewThread3("thirdThread");
	
	Thread t = Thread.currentThread();
	System.out.println("Current Thread is : " + t.getName());
	
	
	try {
		for (int i = 5 ; i > 0 ; i--) {
			System.out.println(" inside thread main Value of i is: " + i);
			Thread.sleep(10000);
		}
		
	}
	catch (InterruptedException e) {
		System.out.println( e );
	}
	System.out.println("Main thread complete");
}
}


class NewThread3 implements Runnable {
	String threadName;
	Thread t ;
	NewThread3(String threadName){
		t = new Thread(this, threadName);
		System.out.println("Inside Child Thread: " + t.getName());
		t.start();
	}
	
	
	@Override
	public void run() {
		
		for (int i = 0; i < 6 ;i ++) {
			System.out.println("Inside thread " + t.getName() + "with i = " + i);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Compltion of thread " + t.getName());
	}
}

