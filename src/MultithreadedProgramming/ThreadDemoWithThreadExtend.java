package MultithreadedProgramming;

public class ThreadDemoWithThreadExtend {
public static void main (String agrs []) {
	new NewThread2();
	
	Thread t = Thread.currentThread();
	
	System.out.println("Current Thread is : " + t.getName());
	
	try {
		for (int i = 5 ; i > 0 ; i--) {
			System.out.println(" inside thread main Value of i is: " + i);
			Thread.sleep(10000);
		}
		
	}
	catch (InterruptedException e) {
		System.out.println( e );
	}
	System.out.println("Main thread complete");
}
}


class NewThread2 extends Thread {
	
	Thread t ;
	NewThread2(){
		super("ExtendThread");
		t = new Thread(this, "Child_Thread");
		System.out.println("Inside Child Thread: " + t.getName());
		t.start();
		
	}
	
	
	@Override
	public void run() {
		System.out.println("After start, inside RUN");
		for (int i = 0; i < 6 ;i ++) {
			System.out.println("Inside thread " + t.getName() + "with i = " + i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(" Child Thread Complete");
	}
}

