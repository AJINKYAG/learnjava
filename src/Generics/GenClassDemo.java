package Generics;

class Gen<T>{
	T obj; 
	
	public void	assignT( T ob) {
		obj = ob;
	}
	
	public void showT(){
		System.out.println("T is type of " + obj.getClass() + "with value of " + obj);
	}
	
}




class GenClassDemo{
	public static void main (String args[]){
		System.out.println("Hello, I am inside Generic Class Demo");
		Gen<Integer> genObj = new Gen<Integer>();
		
		genObj.assignT(88);
		System.out.println("value of T assigned");
		genObj.showT();
		System.out.println("bye bye");
		
	}
}