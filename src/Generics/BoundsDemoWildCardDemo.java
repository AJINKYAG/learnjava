package Generics;

class Stats<T extends Number>{
	T[] nums;
	
	Stats(T[] ob){
		nums = ob;
	}
	
	double average() {
		double avg = 0.0;
		for (int i = 0; i< nums.length; i++)
			avg += nums[i].doubleValue();
		return avg/nums.length;
	}
	
	
	boolean isSameAvg(Stats<?> ob) {
		if(average() == ob.average())
			return true;
		else
			return false;
		
	}
	
	
}

public class BoundsDemoWildCardDemo {
	public static void main(String agrs[]) {
		Integer iArray[] ={1,3,5,7,9};
		
		Double dArray[] = {1.0, 3.0, 5.0, 7.0, 9.0, 11.0};
		
		Float fArray[] = {1.0F, 3.0F, 5.0F, 7.0F, 9.0F};
		
		String sArray[] = {"Ajinkya", "Mukti"};
		
		Stats<Integer> iobj = new Stats<Integer>(iArray);
		Stats<Double> dobj = new Stats<Double>(dArray);
		Stats<Float> fobj = new Stats<Float>(fArray);
		//Stats<String> sobj = new Stats(sArray);  
		//Bound mismatch: The type String is not a valid substitute for the bounded parameter <T extends Number> of the type Stats<T>
		
		System.out.println("Average of Int Array " + iobj.average());
		System.out.println("Avaegra of Double Array " + dobj.average());
		System.out.println("Average of FLoat Array " + fobj.average());
		
		System.out.println("Int and Float Array are same? " + iobj.isSameAvg(fobj));
		System.out.println("Int and Double Array are same? " + iobj.isSameAvg(dobj));
		System.out.println("Double and Float Array are same? " + dobj.isSameAvg(fobj));
	}
	
	
}

