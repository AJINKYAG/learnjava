package Generics;

class TwoD{
	int x, y;
	
	TwoD(int a, int b){
		x = a;
		y=b;
	}
}

class ThreeD extends TwoD{
	int z;
	
	ThreeD(int a, int b, int c){
		super(a,b);
		z = c;
	}
}

class FourD extends ThreeD{
	int t; 
	
	FourD(int a , int b, int c, int d){
		super(a,b, c);
		t = d;
	}
}

class Cordinates<T extends TwoD>{
	T[] coords;
	
	Cordinates(T[] coOrds){
		coords = coOrds;
	}
}

public class BoundedWildCardDemo {

	static void showXY(Cordinates<?> c) {
		System.out.println("showing X and Y co-ordinates-");
		
		for(int i = 0; i < c.coords.length; i++) {
			System.out.println(c.coords[i].x + " " + c.coords[i].y);
		}
		
		System.out.println("________");
		
	}
	
	static void showXYZ(Cordinates<? extends ThreeD> c) {
System.out.println("showing X , Y and Z co-ordinates-");
		
		for(int i = 0; i < c.coords.length; i++) {
			System.out.println(c.coords[i].x + " " + c.coords[i].y +  "  " + c.coords[i].z);
		}
		
		System.out.println("________");
		
	}
	
	static void showALL(Cordinates<? extends FourD> c) {
System.out.println("showing X , Y, Z and T Co-ordinates-");
		
		for(int i = 0; i < c.coords.length; i++) {
			System.out.println(c.coords[i].x + " " + c.coords[i].y +  "  " + c.coords[i].z + "  " + c.coords[i].t);
		}
		
		System.out.println("________");
	}
	
	public static void main (String Args[]) {
		
		TwoD td[] = {
				new TwoD(0,9),
				new TwoD(1,8),
				new TwoD(2,7),
		};
		
		ThreeD thD[] = { new ThreeD(1,2,3),
				new ThreeD(2,3,4),
				new ThreeD (3,4,5)
		};
		
		FourD fd[] = { new FourD(1,2,3,4),
				new FourD(4,5,6,7)
		};
		
		
		Cordinates<TwoD> tdlocs = new Cordinates<TwoD>(td);
		System.out.println("Contents of tdlocs: ");
		showXY(tdlocs);
		//showXYZ(tdlocs);//The method showXYZ(Cordinates<? extends ThreeD>) in the type BoundedWildCardDemo is not applicable for the arguments (Cordinates<TwoD>)
		//showALL(tdlocs); //The method showXYZ(Cordinates<? extends ThreeD>) in the type BoundedWildCardDemo is not applicable for the arguments (Cordinates<TwoD>)
		
		Cordinates<FourD> fdlocs = new Cordinates<FourD>(fd);
		showXY(fdlocs);
		showXYZ(fdlocs);
		showALL(fdlocs);
		
	}
}
