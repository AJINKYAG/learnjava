//convert the ArayList into Array to find the sum
package Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class ArrayListToArray {

	public static void main ( String argsp[]) {
		
		ArrayList<Integer> a1 = new ArrayList<Integer>();
		ArrayList<String> b1 = new ArrayList<String>();
		
		System.out.println("Initial Size of the Interger ArrayList is : " + a1.size());
		 
		a1.add(0);
		a1.add(1);
		a1.add(2);
		a1.add(3);
		a1.add(4);
		a1.add(5);
		
		System.out.println("Current Size of the Interger ArrayList is : " + a1.size());
		
		System.out.println("Containts of the ArrayList is : "  + a1);
		
		int sum = 0;
		for(int i : a1) {
			sum += i;
		}
		System.out.println("Value of Sum : " + sum);
		
		a1.remove(3);
		
		Integer[] array1 = new Integer[a1.size()];
		array1 = a1.toArray(array1);
		
		int Summation = 0;
		for (int i : array1) {
			Summation +=i;
		}
		
		System.out.println("Summation : " + Summation);
		
		System.out.println("ArrayList for Strings");
		
				
		b1.add("Ajinkya");
		b1.add("Gosavi");
		
		String name = "";
		
		System.out.println("String ArrayList b1 " + b1);
		System.out.println("String ArrayList b1 Size: " + b1.size());
		System.out.println("String ArrayList b1 toArray " + b1.toArray());
		
		String sArray = "";
		
		sArray =  b1.toString();
		
		System.out.println("Value of sArray: " +  sArray);
		
		
		for (String i : b1) {
			name += i;
			
		}
		
		System.out.println("Name is : "  + name);
		
		
		b1.add(0,"Muki");
		for (String i : b1) {
			name += i;
			
		}
		
		System.out.println("Name is : "  + name);
	
		LinkedList<Integer> l1 = new LinkedList<Integer>(a1);
		
		System.out.println("The same a1 ArrayList in LinkedList form  : " + l1);
		System.out.println("The Size of LinkedList form  : " + l1.size());
		
		
		l1.add(7);
		l1.add(5, 8);
		l1.addFirst(100);
		l1.addLast(001);
		
		System.out.println("The LinkedList form  : " + l1);
		System.out.println("The Size of LinkedList form  : " + l1.size());
		
		int x = l1.remove(1);
		System.out.println("The LinkedList form  : " + l1 + "with removed value x: " + x);
		
		x= l1.remove();
		System.out.println("The LinkedList form  : " + l1 + "with removed value x: " + x);
		
		System.out.println("index of 8 is : " + l1.indexOf(8));
		
		Object l2 = l1.clone();
		
		System.out.println("Clone of l1 as Object l2: " + l2);
		
		System.out.println("Class of l1 is "+ l1.getClass());
		
		l1.clear();
		
		System.out.println("clearing l1 returns : " + l1);
		
		System.out.println("l1 isEMpty : " +l1.isEmpty() );
		
		l1=(LinkedList<Integer>) l2;
		
		Iterator<Integer> x1 =l1.iterator();
		while(x1.hasNext()) {
			System.out.println(x1.next());
		}
		
		x1 =l1.descendingIterator();
		
		System.out.println("after decending iterator");
		
		while(x1.hasNext()) {
			System.out.println(x1.next());
		}
	}
	
}
