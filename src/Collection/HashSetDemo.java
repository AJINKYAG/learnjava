package Collection;

import java.util.HashSet;
import java.util.LinkedHashSet;
public class HashSetDemo {
	
	public static void main (String agrs[]) {
		System.out.println("inside main");
		
		HashSet<String> hs = new HashSet<String>();
		
		hs.add("Ajinkya");
		hs.add("Arti");
		hs.add("Madhuri");
		hs.add("Ulhas");
		hs.add("Mukti");
		
		System.out.println("Printing hashset hs");
		System.out.println(hs);
		
		System.out.println("total no of elements in hashset: " + hs.size());
		
		System.out.println("Is Manjiri part of hashset ? : " + hs.contains("Manjiri"));
		
		hs.add("Manjiri");
		System.out.println("Manjiri addded to Hashset : " + hs.contains("Manjiri"));
		System.out.println(" hashset : " + hs);
		hs.remove("Manjiri");
		
		System.out.println("trying to add Ajinkya again to hs : " + hs.add("Ajinkya"));
		
		System.out.println("Final hashset : " + hs);
		
		
		LinkedHashSet<String> lhs1 = new LinkedHashSet<String>(hs);
		
		System.out.println("LinkedHashSet lhs1 has : " + lhs1);
		
		LinkedHashSet< String> lhs2 = new LinkedHashSet<String>(8);
		System.out.println("Size of lhs2 : " + lhs2.size());
		
		lhs2.add("Ajinkya");
		lhs2.add("Arti");
		lhs2.add("Madhuri");
		lhs2.add("Ulhas");
		lhs2.add("Mukti");
		
		System.out.println("Capacity of Lhs2 : " + lhs2.size());
		System.out.println(" lhs2 contains :  " + lhs2);
		System.out.println("To String method for lhs2: " + lhs2.toString());
		lhs2.add("Kalpana");
		lhs2.add("Ajay");
		lhs2.add("Kasturi");
		System.out.println("Capacity of Lhs2 : " + lhs2.size());
		System.out.println(" lhs2 contains :  " + lhs2);
		System.out.println("Trying to add 9th Name Murari : " + lhs2.add("Murari "));
		System.out.println("Capacity of Lhs2 : " + lhs2.size());
		System.out.println(" lhs2 contains :  " + lhs2);
		System.out.println("What is teh float ration for lhs2: "  );
		
		lhs2.forEach( System.out::println);
		
		LinkedHashSet<String> lhs3 = new LinkedHashSet<String>(2,(float) 0.50);
		System.out.println("Size of lhs3 : " + lhs3.size());
		System.out.println("lhs3  Contains: " + lhs3);
		lhs3.add("A");
		System.out.println("Size of lhs3 : " + lhs3.size());
		System.out.println("lhs3  Contains: " + lhs3);
		lhs3.add("B");
		System.out.println("Size of lhs3 : " + lhs3.size());
		System.out.println("lhs3  Contains: " + lhs3);
		
		
		
	}
}
