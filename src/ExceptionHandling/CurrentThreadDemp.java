package ExceptionHandling;

public class CurrentThreadDemp {

	public static void main(String args[]) {
		
		Thread t = Thread.currentThread();
		System.out.println("Current Thread is : "  + t);
		
		t.setName("My Main Thread");
		System.out.println("After name change : " + t);
		
		
		try {
			for (int n = 0;n< 5;n++) {
				System.out.println("State of Thread is: " + t.getState());
				System.out.println("N is: " + n);
				Thread.sleep(2000);
				System.out.println("State of Thread is: " + t.getState());
				
			}
		}
		catch (InterruptedException e)
		{
			System.out.println("Received Exception : " + e);
		}
		
		
	}
	
}
