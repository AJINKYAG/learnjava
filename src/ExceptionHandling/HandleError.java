package ExceptionHandling;

import java.util.Random;

class HandleError{
	public static void main(String args[]) {
		int a,b,c=0;
		Random r = new Random();
		
		for (int i=0;i<3200;i++) {
			try {
				b=r.nextInt();
				c=r.nextInt();
				a=1234/(b/c);
			}
			catch(ArithmeticException e) {
				System.out.println("The exception might be because of b or c is 0. Assigning a = 0" + e);
				a=0;
			}
			if (a==0) 
				break;
			else
			System.out.println("Value of a is : " + a);
		}
		
	}
}