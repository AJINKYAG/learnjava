package ExceptionHandling;

class Trycatch{
	
	
	private static int devide(int x, int y) throws ArithmeticException {
		int a =0;
		try {
		 a=x/y;
		}
		catch (Exception e ){
			System.out.println("This is the catch method with Stack trace e " + e);
		}
		return a ;
	}
	
	public static void main(String args[]) {
		int a= devide(40,0);
		System.out.println("value of a is : " + a);
			
	}
}