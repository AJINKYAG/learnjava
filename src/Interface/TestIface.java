package Interface;

class TestIface {
	public static void main(String args[]) {
		Callback c1 = new Client();
		c1.callback(9765900308L);
		
		Callback c2 = new AnotherClient();
		c2.callback(9786900308L);
		
		c1 = c2;
		c1.callback(9765900308L);
	}
}