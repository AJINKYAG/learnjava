package Interface;

class NestedIfDemo{
	public static void main (String Args[]) {
		B b = new B();
		b.isNotNegative(10);
		
		A.NestedIf nif = new B();
		
		if(nif.isNotNegative(10))
			System.out.println("Value is positive ");
		if(nif.isNotNegative(-4))
			System.out.println("Code will never go here");
		
	}
}   