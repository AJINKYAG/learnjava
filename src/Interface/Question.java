package Interface;

import java.util.Random;

class Question implements SharedConstants{
	Random random = new Random();
	
	int ask () {
		int prob = (int) (100 * random.nextDouble());
		if (prob < 30)
			return NO;
		if (prob < 60)
			return YES;
		if (prob < 75)
			return MAYBE;
		if (prob < 95)
			return LATER;
		else
			return NEVER;
		
	}
	
	
}

