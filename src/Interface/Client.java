package Interface;

class Client implements Callback{

	@Override
	public void callback(long phoneNumber1) {
		//Implemented Callback interfaced public method
		//you can noy add more parameters to this method as this is what he interface gave
		//if you tried to add more, it will be new method of class Client and not the overridden method of interface
		
		System.out.println("Calling with the Phonenumber in Client class: " + phoneNumber1) ;
		
	}
	
	public  int nonOverriddenMethod() {
		System.out.println("This is a non overridden method of class Client");
		return 1;
		
	}
	
}