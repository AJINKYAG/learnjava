package Interface;

//define an integer stack interface
interface IntStack{
	int myValue = 0;
	void push(int item); //push an integer item in to the stack
	int pop(); // pop out the latest item from stack
}