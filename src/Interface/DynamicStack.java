package Interface;

class DynamicStack implements IntStack{

	private int stack[];
	private int tos;
	private int stackLength;
	
	DynamicStack(int size){
		stack = new int[size];
		tos = -1;
		stackLength = stack.length;
	}
	
	@Override
	public void push(int item) {
		if (tos == stack.length -1) {
			int temp[] = new int [stackLength*2];
			for (int i = 0 ; i < stackLength; i++) {
				temp[i] = stack[i];
			}
			stack = temp;
			stackLength = stack.length;
			System.out.println("new Stack length is : " +stackLength );
			stack[++tos] = item;
			
		}
		else
			stack[++tos]=item;
		
	}

	@Override
	public int pop() {
		if (tos < 0) {
			System.out.println("sTACK IS EMPTY");
			return 0;
		}
			
		
		else {
			return stack[tos--];
		}
	}
	
}

