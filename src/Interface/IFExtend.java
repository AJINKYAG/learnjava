package Interface;

interface AA {
	void method1();
	void method2();
	
}

interface BB extends AA {
	void method3();
	
}


class CC implements BB{

	@Override
	public void method1() {
		System.err.println("Inside Method 1");
		
	}

	@Override
	public void method2() {
		System.err.println("Inside Method 2");
		
	}

	@Override
	public void method3() {
		System.err.println("Inside Method 3");
	}
	
}



class IFExtend extends CC{
	public static void main (String args[]) {
		CC cc = new CC();
		cc.method1();
		cc.method2();
		cc.method3();
	}
}