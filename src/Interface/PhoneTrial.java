package Interface;

public class PhoneTrial implements Callback{

	@Override
	public void callback(long phoneNumber) // this method must be public 
	{
		System.out.println("Override the interface method of callbac where Method parameter is final and staic meaning you can not change it. " +phoneNumber  );
		
	}
	
	void call(long phoneNumber) {
		System.out.println("Inside call method of class with Phone # " + phoneNumber);
	}

}
