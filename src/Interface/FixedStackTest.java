package Interface;

class FixedStackTest{
	
	public static void main (String args[]) {
		FixedStack stack1 = new FixedStack(8);
		FixedStack stack2 = new FixedStack(5);
		
		for (int i = 0; i <stack1.getStackLength(); i++) {
			stack1.push(i+10);
		}
		for (int i = 0; i <stack2.getStackLength(); i++) {
			stack2.push(i+10);
		}
		
		System.out.println("Stack 1 ");
		for (int i = 0; i <stack1.getStackLength(); i++) {
			System.out.println(stack1.pop());
		}
		
		System.out.println("Stack 2 ");
		
		for (int i = 0; i <stack2.getStackLength(); i++) {
			System.out.println(stack2.pop());
		}
		
		DynamicStack dynamicStack1 = new DynamicStack(8);
		System.out.println("Input to dynamic Stack");
		for(int i = 0; i< 8*1.5 ; i++) {
			dynamicStack1.push(i*2);
		}
		
		System.out.println("Output from dynamic stack");
		
		for(int i = 0; i< 8*1.5 ; i++) {
			System.out.println(dynamicStack1.pop());
		}
		
		System.out.println("\n\n Accessing stack through Interface Variable");
		
		IntStack is1 = new FixedStack(5);
		IntStack is2 = new DynamicStack(5);
		System.out.println("Input to Static Stack");
		for(int i = 0; i< 8*1.5 ; i++) 
		{
			is1.push(i*2);
		}
		
		System.out.println("Output from static stack");
		
		for(int i = 0; i< 8*1.5 ; i++) {
			System.out.println(is1.pop());
		}
		
		System.out.println("Input to Dynamic Stack");
		for(int i = 0; i< 8*1.5 ; i++) 
		{
			is2.push(i*2);
		}
		
		System.out.println("Output from Dynamic stack");
		
		for(int i = 0; i< 8*1.5 ; i++) {
			System.out.println(is2.pop());
		}
		
	}
	
}