package Interface;

class FixedStack implements IntStack{

	private int stack[];
	private int tos;
	public int StackLength;
	
	public int getStackLength() {
		return StackLength;
	}

	FixedStack(int size){
		stack= new int[size];
		tos = -1;
		StackLength = size;
	}
	
	@Override
	public void push(int item) {
		if(tos == stack.length -1)
			System.out.println("The stack is full");
		else 
			stack[++tos] = item;
		
	}

	@Override
	public int pop() {
		if (tos < 0) {
			System.out.println("sTACK IS EMPTY");
			return 0;
		}
			
		
		else {
			return stack[tos--];
		}
			
		
	}
	
}