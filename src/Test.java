
public class Test {

	public static void main (String args[]) {
		byte x = 127;
		byte y = 127;
		//byte Z = x + y;//Type mismatch: cannot convert from int to byte
		int z = x+y;
		
		// System.out.println(Z); //Type mismatch: cannot convert from int to byte
		System.out.println(z);
	}
}
